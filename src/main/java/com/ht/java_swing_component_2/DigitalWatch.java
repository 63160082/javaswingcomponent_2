/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class DigitalWatch implements Runable {
    JFrame frame;
    Thread th = null;
    
    int hours = 0, minutes = 0, seconds = 0;
    String timeString = "";
    JButton btn;

    DigitalWatch() {
        frame = new JFrame();
    th = new Thread((Runnable) this);
        th.start();

        btn = new JButton();
        btn.setBounds(100, 100, 100, 50);

        frame.add(btn);
        frame.setSize(300, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void run() {
        try {
            while (true) {

                Calendar cal = Calendar.getInstance();
                hours = cal.get(Calendar.HOUR_OF_DAY);
                if (hours > 12) {
                    hours -= 12;
                }
                minutes = cal.get(Calendar.MINUTE);
                seconds = cal.get(Calendar.SECOND);

                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
                Date date = cal.getTime();
                timeString = formatter.format(date);

                printTime();

                th.sleep(1000);  
            }
        } 
        catch (Exception e) {
        }
    }

    public void printTime() {
        btn.setText(timeString);
    }

    public static void main(String[] args) {
        new DigitalWatch();

    }
}
    
