/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author ACER
 */
public class ToolBar {
    public static void main(String[] args) {
        JFrame myFrame = new JFrame("JToolBar Example");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);
        
        JButton btn = new JButton("File");
        toolbar.add(btn);
        toolbar.addSeparator();
        toolbar.add(new JButton("Edit"));
        toolbar.add(new JComboBox(new String[] { "Opt-1", "Opt-2", "Opt-3", "Opt-4" }));  
        
        Container contentPane = myFrame.getContentPane();  
        contentPane.add(toolbar, BorderLayout.NORTH);  
        
        JTextArea textArea = new JTextArea();  
        JScrollPane mypane = new JScrollPane(textArea);  
        //contentPane.add(mypane, BorderLayout.EAST);  
        myFrame.setSize(450, 250);  
        myFrame.setVisible(true);  
    }
}
