/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

/**
 *
 * @author ACER
 */
public class JRootPane {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        javax.swing.JRootPane root = frame.getRootPane();
        
        JMenuBar bar = new JMenuBar();
        JMenu menu = new JMenu();
        
        bar.add(menu);
        menu.add("Open");
        menu.add("Close");
        
        root.setJMenuBar(bar);
        root.getContentPane().add(new JButton("Press Me"));
        
        frame.pack();
        frame.setVisible(true);
    }
}
