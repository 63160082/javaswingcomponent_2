/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class DisplayingImage {
    public void paint(Graphics graphics) {
        Toolkit toolKit = Toolkit.getDefaultToolkit();
        Image img = toolKit.getImage("p3.gif");
        graphics.drawImage(img, 120, 100, (ImageObserver) this);
    }

    public static void main(String[] args) {
        DisplayingImage disImage = new DisplayingImage();
        JFrame frame = new JFrame();
        
        frame.setSize(400, 400);
        frame.setVisible(true);
    } 
}
