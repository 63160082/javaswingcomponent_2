/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author ACER
 */
public class TitleIcon {
    TitleIcon() {
        Frame frame = new Frame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\icon.png");
        
        frame.setIconImage(icon);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
    
    public static void main(String[] args) {
        new TitleIcon();
    }
}
