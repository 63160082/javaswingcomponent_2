/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

/**
 *
 * @author ACER
 */
public class ToggleButton extends JFrame implements ItemListener {
    public static void main(String[] args) {
        new ToggleButton();
    }
    
    private JToggleButton btn;
    ToggleButton() {
        setTitle("JTggleButton with ItemListener Example");
        setLayout(new FlowLayout());
        setToggleButton();  
        setAction();  
        setSize(200, 200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
        private void setToggleButton() {  
        JToggleButton button = new JToggleButton("ON");  
        add(button);  
    }  
        
       private void setAction() {  
        button.addItemListener(this);  
    }  
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
