/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class FileChooser extends JFrame implements ItemListener {

    JMenuBar mBar;
    JMenu file;
    JMenuItem open;
    JTextArea txtArea;

    FileChooser() {
        open = new JMenuItem("Open File");
        open.addActionListener((ActionListener) this);
        
        file = new JMenu("File");
        file.add(open);
        
        mBar = new JMenuBar();
        mBar.setBounds(0, 0, 800, 20);
        mBar.add(file);
        
        txtArea = new JTextArea(800, 800);
        txtArea.setBounds(0, 20, 800, 800);
        
        add(mBar);
        add(txtArea);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == open) {
            JFileChooser fc = new JFileChooser();
            int i = fc.showOpenDialog(this);
            if (i == JFileChooser.APPROVE_OPTION) {
                File f = fc.getSelectedFile();
                String filepath = f.getPath();
                try {
                    BufferedReader br = new BufferedReader(new FileReader(filepath));
                    String s1 = "", s2 = "";
                    while ((s1 = br.readLine()) != null) {
                        s2 += s1 + "\n";
                    }
                    txtArea.setText(s2);
                    br.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        FileChooser om = new FileChooser();
        om.setSize(500, 500);
        om.setLayout(null);
        om.setVisible(true);
        om.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }


}
