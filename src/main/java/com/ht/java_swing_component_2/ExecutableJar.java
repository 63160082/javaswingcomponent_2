/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component_2;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class ExecutableJar {
    ExecutableJar() {
        JFrame frame = new JFrame();

        JButton button = new JButton("click");
        button.setBounds(130, 100, 100, 40);

        frame.add(button);
        frame.setSize(300, 400);
        frame.setLayout( null);
        frame.setVisible( true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new ExecutableJar();
    }
}
